<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get( '/', 'PagesController@index' );
Route::get( 'about', 'PagesController@about' );
Route::get( 'contact', 'PagesController@contact' );

Route::controllers( [
	'auth'     => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
] );

Route::get( 'file-upload', 'FileUpload\FileUploadController@index' );
Route::get( 'file-upload/create', 'FileUpload\FileUploadController@create' );
Route::post( 'file-upload', 'FileUpload\FileUploadController@store' );

Route::get( 'file-parse/{id}', 'FileParserController@index' );