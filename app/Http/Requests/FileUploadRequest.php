<?php namespace App\Http\Requests;


class FileUploadRequest extends Request
{

	/**
	 * Determine if the user is authorized to make this request.
	 * @return bool
	 * @internal param \Illuminate\Auth\Guard $auth
	 *
	 */
	public function authorize()
	{
		return \Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'file' => 'required|mimes:csv,xls,xlsx'
		];
	}

	public function messages()
	{
		return [
			'file.required' => 'File field is required.',
			'file.mimes'	=> 'Bad file type, please upload and Excel or CSV file',
		];
	}

}
