<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'reports';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'name' ];

	/**
	 * A report belongs to a file
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function files()
	{
		return $this->belongsToMany('App\File');
	}

	/**
	 * A file belongs to many users
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function users()
	{
		return $this->belongsToMany('App\User');
	}
}
